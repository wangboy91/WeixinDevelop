﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeixinPayWeb.Models
{
    public class WeiXinPayResponseModel
    {
        /// <summary>
        /// 应用Id
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        public string PartnerId { get; set; }
        /// <summary>
        /// 预支付交易会话
        /// </summary>
        public string PrepayId { get; set; }
        /// <summary>
        /// 扩展字段
        /// </summary>
        public string PackageValue { get; set; }
        /// <summary>
        /// 随机字符串
        /// </summary>
        public string NonceStr { get; set; }
        /// <summary>
        /// 时间戳
        /// </summary>
        public string TimeStamp { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string Sign { get; set; }
        /// <summary>
        /// 交易订单序列号 服务端生成
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 支付金额
        /// </summary>
        public decimal PayAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public WeiXinPayResponseModel()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="parnerId"></param>
        /// <param name="prepayId"></param>
        /// <param name="packAge"></param>
        /// <param name="noncestr"></param>
        /// <param name="timesTamp"></param>
        /// <param name="sign"></param>
        /// <param name="transactionId"></param>
        /// <param name="payAmount"></param>
        public WeiXinPayResponseModel(string appId, string parnerId, string prepayId, string packAge, string noncestr, string timesTamp, string sign, string transactionId, decimal payAmount)
        {
            this.AppId = appId;
            this.PartnerId = parnerId;
            this.PrepayId = prepayId;
            this.PackageValue = packAge;
            this.NonceStr = noncestr;
            this.TimeStamp = timesTamp;
            this.Sign = sign;
            OrderId = transactionId;
            PayAmount = payAmount;
        }
    }
}
