﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WeixinPayWeb
{
    public class HostLogHelper
    {
        public static void AddLog(string txt)
        {
            var logtxtPath =AppDomain.CurrentDomain.BaseDirectory +"\\wexinPay.txt";
            if (!File.Exists(logtxtPath))
            {

                FileStream txtFile = new FileStream(logtxtPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                StreamWriter sw = new StreamWriter(txtFile, System.Text.Encoding.Unicode);
                sw.WriteLine(txt);//开始写入值
                sw.Close();
                txtFile.Close();

            }
            else
            {
                FileStream txtFile = new FileStream(logtxtPath, FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(txtFile, System.Text.Encoding.Unicode);
                sw.WriteLine(txt);//开始写入值
                sw.Close();
                txtFile.Close();
            }
        }

        public static void AddLog(string txt, params object[] args)
        {
            var msg = string.Format(CultureInfo.InvariantCulture, txt, args);
            AddLog(msg);

        }
    }
}
