﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WBoy.WeixinCore.Exceptions;

namespace WeixinPayWeb.Controllers
{
    [Route("api/Weixin")]
    [ApiController]
    public class WeixinController : ControllerBase
    {
        private readonly WBoy.WeixinCore.AuthHelper _weiXinAuthHelper;

        public WeixinController()
        {
            _weiXinAuthHelper = new WBoy.WeixinCore.AuthHelper("", "");
        }

        [HttpPost]
        [ActionName("GetWXsignature")]
        public ResultMessage GetWXsignature(string noncestr, string timestamp, string pageurl)
        {
            try
            {
                var jsSignature = _weiXinAuthHelper.GetSignature(noncestr, timestamp, pageurl);
                return new ResultMessage<WBoy.WeixinCore.Models.JsSignature>(jsSignature);
            }
            catch (WeixinException e)
            {
                return new ResultMessage(StateCode.Fail, e.Message);
            }
        }
        ///// <summary>
        ///// 微信后台验证地址（使用Get），
        ///// </summary>
        //[HttpGet]
        //[ActionName("Index")]
        //public ActionResult Get(PostModel postModel, string echostr)
        //{

        //    if (_weiXinAuthHelper.CheckSignature(postModel.Signature, postModel.Timestamp, postModel.Nonce))
        //    {
        //        return Content(echostr); //返回随机字符串则表示验证通过
        //    }
        //    else
        //    {
        //        return Content("failed:" + postModel.Signature + "。" +
        //                       "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。");
        //    }
        //}
    }
}