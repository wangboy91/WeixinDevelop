﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace WBoy.WeixinCore
{
    public class WebRequestHelper
    {
        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true; //总是接受   
        }

        #region Get请求服务器数据
        public string RequestGetDataUrl(string url)
        {
            string strResponse = "";
            string targeturl = url.Trim();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(targeturl);
            request.Method = "GET";
            request.KeepAlive = true;
            request.ContentType = "application/x-www-form-urlencoded";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (Stream resStream = response.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(resStream, System.Text.Encoding.GetEncoding("utf-8")))
                {
                    strResponse = sr.ReadToEnd();
                }
            }
            return strResponse;
        }
        #endregion

        #region Post请求服务器数据
        public string RequestPostDataUrl(string url, string strPostDatas)
        {
            string strResponse = "";

            string targeturl = url.Trim().ToString();
            //如果是发送HTTPS请求   
            HttpWebRequest request = null;
            if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
            {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                request = WebRequest.Create(url) as HttpWebRequest;
                if (request != null) request.ProtocolVersion = HttpVersion.Version10;
            }
            else
            {
                request = WebRequest.Create(url) as HttpWebRequest;
            }
            //HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(targeturl);
            if (request != null)
            {
                request.Method = "POST";
                request.KeepAlive = true;
                request.ContentType = "application/x-www-form-urlencoded";
                if (!string.IsNullOrEmpty(strPostDatas))
                {
                    var postDatas = Encoding.GetEncoding("utf-8").GetBytes(strPostDatas);

                    request.ContentLength = postDatas.Length;
                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(postDatas, 0, postDatas.Length);
                    }
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream resStream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(resStream, System.Text.Encoding.GetEncoding("utf-8")))
                    {
                        strResponse = sr.ReadToEnd();
                    }
                }
            }
            return strResponse;

        }
        #endregion
    }
}
