﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore.Models
{
    /// <summary>
    /// AccessToken
    /// </summary>
    public class AccessToken
    {
        public string Access_token { set; get; }
        public int Expires_in { set; get; }
    }
}
