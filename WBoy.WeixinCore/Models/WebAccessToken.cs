﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore.Models
{
    /// <summary>
    /// 网站授权AccessToken
    /// </summary>
    public class WebAccessToken
    {
        public string Access_token { set; get; }
        public int Expires_in { set; get; }
        public string Refresh_Token { get; set; }
        public string Openid { get; set; }
        public string Scope { get; set; }
        public string Unionid { get; set; }
    }
}
