﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore.Models
{
    /// <summary>
    /// 微信用户信息
    /// </summary>
    public class WeiXinUserInfo
    {
        public int? Subscribe { get; set; }
        public string Openid { get; set; }

        public string Nickname { get; set; }
        public string Sex { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Headimgurl { get; set; }
        public List<string> Privilege { get; set; }

        public string Unionid { get; set; }

    }
}
