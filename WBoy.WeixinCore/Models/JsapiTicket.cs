﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore.Models
{
    /// <summary>
    /// 微信JS-SDK使用权限签名的Jsapi_ticket
    /// </summary>
    public class JsApiTicket
    {
        public string Ticket { set; get; }
        public int Expires_in { set; get; }
    }
}
