﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore.Models
{
    /// <summary>
    /// jsSDK签名
    /// </summary>
    public class JsSignature
    {
        public string Jsapi_ticket { set; get; }
        public string Signature { set; get; }
    }
}
