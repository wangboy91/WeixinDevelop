﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore.Models
{
    /// <summary>
    /// 错误信息
    /// </summary>
    public class ErrorMsg
    {
        public int Errcode { set; get; }
        public string Errmsg { set; get; }
    }
}
