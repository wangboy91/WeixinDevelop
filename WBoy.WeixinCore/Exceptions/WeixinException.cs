﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WBoy.WeixinCore.Exceptions
{
    public class WeixinException : Exception
    {
        public int ErrorCode { private set; get; }

        public WeixinException(int code,string message) :
            base(message)
        {
            ErrorCode = code;
        }
    }
}
