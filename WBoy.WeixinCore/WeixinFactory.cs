﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WBoy.WeixinCore.Models;

namespace WBoy.WeixinCore
{
    public class WeixinFactory
    {
        private static AuthHelper _authHelper;
        public static AuthHelper CreateAuthHelper()
        {
            return _authHelper ?? (_authHelper = new AuthHelper(Configure.AppId, Configure.AppSecret,Configure.Token));

        }
    }
}
