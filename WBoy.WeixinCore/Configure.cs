﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBoy.WeixinCore
{
    /// <summary>
    /// 微信配置
    /// </summary>
    public static class Configure
    {
        //这个就是自己要保管好的私有Key了（切记只能放在自己的后台代码里，不能放在任何可能被看到源代码的客户端程序中）
        //每次自己Post数据给API的时候都要用这个key来对所有字段进行签名，生成的签名会放在Sign这个字段，API收到Post数据的时候也会用同样的签名算法对Post过来的数据进行签名和验证
        //收到API的返回的时候也要用这个key来对返回的数据算下签名，跟API的Sign数据进行比较，如果值不一致，有可能数据被第三方给篡改

        //微信分配的公众号ID（开通公众号之后可以获取到）

         

        public static string AppId { get; set; }
        public static string AppSecret { get; set; }
        public static string Token { get; set; }

    }
    
}
